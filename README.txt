###############################################################################################################################
Using the Model:
###############################################################################################################################
-Open 'Master.py' in a python editor such as spyder, IDLE etc.
###############################################################################################################################
-Scroll to bottom of script
###############################################################################################################################
-Define the array to be modelled with commmand sim = RingSim()

	The only essential argument is RingNum, the number of rings to be modelled, must be a square number, so 
often useful to define this as number of rings in a row **2. E.g. to model a 5x5 array, type " sim = RingSim(5**2) "

	The array can be initialised with different properties by asserting the following arguments after the 
RingNum. In order to change these parameters, type the parameter name, followed by an equals sign and the value you
wish to set it to, for example, to initialise a 25x25 array with a base energy barrier of 1.7e-19J and a temperature
of 350 Kelvin, you would type " sim = RingSim(25**2, BaseE0=1.7e-19, kT = (350 * 1.38e-23)) ". The names of changeable
parameters, as well as their default values, are listed below:
1- BaseE0,  the energy barrier associated with pinning. Fitting to experimental data for arrays of 4um diameter
400nm track width gives a standard value of 1.68e-19 J. 
2- H0, the zero kelvin switching field of the magnetic structure. Fitting to the same experimental data gives a 
value of 142.5 Oe.
3- Alpha, a parameter that influences magnetic switching behaviour. Fitting to data gives a value of 1.1
4- Tau0, the inverse of the material's attempt frequency. taken to be 1/10GHz.
5- kT, Temperature of the system in Kelvin multiplied by the Boltzmann constant. Assumed to be at room temperature, so 
kT = 298 * 1.38e-23
6- RingSize, The number of possible DW locations per ring ***DO NOT CHANGE THIS FROM 16, CAUSES UNSTABLE BEHAVIOUR***
7- PinCoupling, A modifier to change DW behaviour when 2 DWs share a junction. Fitting gives a value of 0.75.
8- Frequency, The defined frequency of rotation of magnetic field. Taken to be 0.2 following earlier experimental procedures
9- HDist, The standard deviation of H0 for each ring. The effective H0 is modelled as a normal distribution with
mean H0 and standard deviation of HDist. Fitted to be 12.5Oe.
10- FieldEccentricity, allows elliptical fields to be applied. The ellipse is calculated via formula (x/a)^2 + (y/b)^2 = 1.
Eccentricity is calculated as sqrt(1 - b^2/a^2). a is taken to be the maximum strength of applied field. To apply a
field with semi-major of 90Oe in one direction and semi-minor of 60Oe, eccentricity would be calculated as:
e = sqrt(1 - 60^2/90^2) = 0.7454. To enter this into a simulation of a 20x20 array:
" sim = RingSim(20**2, FieldEccentricity=0.7454). The field magnitude for later functions is always entered as the 
semi-major value, so in this case 90. This will become more clear later.
###############################################################################################################################
-Enter what type of experiment you wish to perform. The following functions will be described with required arguments,
as well as brief description of what each experiment measures, the form of the outputs, and an example of a command
to call the functions.

1- imgoutput(duration, H_input, ticksperimage). Outputs .eps files of array state, with all DWs and magnetic direction
of each ring visible. Not recommended to run for arrays above 8x8. Not recommended for duration/ticksperimage greater than 50 on
windows systems due to memory issues on windows systems. Running in linux allows unlimited duration. To run an experiment that gives
an image every 4 ticks (1/4 rotation of the field) with the 90/60 elliptical field described above, for 10 rotations on an 8x8 array,
type the following commands at the bottom of 'Master.py':
" sim = RingSim(8**2, FieldEccentricity=0.7454)
  sim.imgoutput(10*16, 90, 4) "
then run the script.

2- TimeResolvedFieldChange(H1, H2, duration=1600). Outputs a signal of how the array responds over time to two field magnitudes, giving 
normalised data on both DW count, and Y magnetisation of the array as columns 0 and 1 of a csv file. H1 is initial field 
strength, H2 secondary field strength, and duration the number of ticks per experiment, default 1600. Field strenght changes at
time duration/2. To measure the change in system state from a circular driving field of 55Oe to 65Oe on a 25x25 array for 8 rotations
per field:
" sim = RingSim(25**2)
  sim.dataoutputnodes(55, 65, duration=2*8*16)"
then run the script.

3- FieldRangeSweep(Field, NoR). Outputs a plot of end conditions after a given number of rotations (NoR) of a range of fields, defined
as a vector of arbitrary length, renucleating between each applied field. Outputs separate .csv files for Y magnetisation and DW count
at end of field sequence. To get the reponse of a system for fields between 20 and 90 Oe with 11 steps on a 25x25 array, 30 rotations
for each applied field:
" sim = RingSim(25**2)
  Sequence = np.linspace(20, 90, 11)
  sim.dataoutputField(Sequence, 30)"
then run the script.

4- bigimgoutput(duration, H_input, qperimg). Altered image output designed to handle large arrays > 8x8. Can only output images every quarter
rotation, defined by qperimg. ***Only runs on linux, will crash windows systems!*** To output an image of a 30x30 array, every half rotation
for 30 rotations, for an applied field of 57Oe:
" sim = RingSim(30**2)
  sim.bigimgoutput(30*16, 57, 2)"
then run the script.

5- SignalTransform(signal, Centre, Range, tpi, tpo). Modulates the strength of the applied field according to 1D input vector 'signal'. Field is
scaled according to 
Happ = Hc + Hr * input, 
where H is applied field, Hc is Centre, and Hr is Range. 
Tpi manages number of 'ticks' per input. Sticking to multiples of 4 is best as it matches the symmetry of the model, a tpi of 16 means each input
is applied for one rotation. Tpo manages number of 'ticks' per output. tpo must be less than tpi, and will lead the model to create a 4x1D output 
of the length of the input signal * (tpo/tpi). In order, each row of the output will correspond to a metric of the system:
Row 0 = Domain wall population
Row 1 = Magnetisation magnitude
Row 2 = Magnetisation component along X. Right = positive x
Row 3 = Magnetisation component along Y. Up = positive y
Array starts initialised in positive Y and field rotates anticlockwise.

6- DelayedMagFeedback(Signal, Centre, Range, tpi, tpo, Delay, DelayLineMult). As above but with additional parameters simulating delayed
feedback of output. Delay = number of ticks by which to delay feedback of output. DelayLineMult = scaling factor that effects feedback strength.
Feedback here is taken from the measured net magnetisation of the array 'Delay' ticks in the past, changing input equation to:
Happ = Hc + Hr (input(t) + magoutput(t-Delay))

###############################################################################################################################
To recap:
1- Initialise array
2- Define function parameters
3- Run script

Any questions feel free to e-mail 'ITVidamour1@sheffield.ac.uk'!
 
  


